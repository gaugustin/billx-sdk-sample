﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Billx.Api;
using Billx.Api.Model;
using Billx.Sample.Utilities;
using log4net.Appender;
using log4net;
using log4net.Repository.Hierarchy;
using System.Diagnostics;
using log4net.Repository;
using System.Collections;

namespace Billx.Sample
{
    public partial class Form1 : Form
    {
        IBillxService _svc;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                var token = Configuration.GetAPIContext().AccessToken;

                this.textBox3.Text = token.Trim();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                MessageBox.Show(Configuration.GetTokenValidation(this.textBox3.Text).ToString());
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                _svc = new BillxService(this.textBox3.Text.Trim(), BillxResource.WebApi.OTHERS);

                var source = _svc.DownloadInvoices();

                if (source != null)
                {
                    this.dataGridView1.AutoGenerateColumns = true;
                    this.dataGridView1.DataSource = source.ToList();
                }
                else
                {
                    MessageBox.Show("No data found");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                var invoices = new InvoiceModel
                  {
                      InvoiceId = "21EC2020-3AEA-4069-A2DD-08002B30309D",
                      InvoiceTotal = 9,
                      TxnDate = Convert.ToDateTime("2016-01-01 08:09:03"),
                      InvoiceDetails = new List<InvoiceDetailsModel>
                      {
                          new InvoiceDetailsModel { InvoiceId ="21EC2020-3AEA-4069-A2DD-08002B30309D", TxnLineId ="22EC2020-3AEA-4069-A2DD-08002B30309D"}
                      }
                  };

                _svc = new BillxService(this.textBox3.Text.Trim(), BillxResource.WebApi.OTHERS);

                if (_svc.UploadInvoice(invoices))
                    MessageBox.Show("Upload sucessfully");
                else
                    MessageBox.Show("Unable to update, check log");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                var invoices = new List<InvoiceModel>()               
                {
                  new InvoiceModel 
                  {
                      InvoiceId = "21EC2020-3AEA-4069-A2DD-08002B30309D",
                      InvoiceTotal = 9,
                      TxnDate = Convert.ToDateTime("2016-01-01 10:09:03"),
                      InvoiceDetails = new List<InvoiceDetailsModel>
                      {
                          new InvoiceDetailsModel { InvoiceId ="21EC2020-3AEA-4069-A2DD-08002B30309D", TxnLineId ="22EC2020-3AEA-4069-A2DD-08002B30309D"}
                      }
                  },
                  new InvoiceModel 
                  {
                      InvoiceId = "23EC2020-3AEA-4069-A2DD-08002B30309D",
                      InvoiceTotal = 12,
                      TxnDate = Convert.ToDateTime("2016-01-01 11:09:03"),
                      InvoiceDetails = new List<InvoiceDetailsModel>
                      {
                          new InvoiceDetailsModel { InvoiceId ="23EC2020-3AEA-4069-A2DD-08002B30309D", TxnLineId ="24EC2020-3AEA-4069-A2DD-08002B30309D"}
                      }
                  },

                };

                _svc = new BillxService(this.textBox3.Text.Trim(), BillxResource.WebApi.OTHERS);

                if (_svc.UpdateInvoices(invoices))
                    MessageBox.Show("Updated sucessfully");
                else
                    MessageBox.Show("Unable to update, check log");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                _svc = new BillxService(this.textBox3.Text.Trim(), BillxResource.WebApi.OTHERS);

                if (_svc.UpdateInvoiceStatus(12, "Import To - Software Name", "R-001"))
                    MessageBox.Show("Updated sucessfully");
                else
                    MessageBox.Show("Unable to update, check log");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                var paymentinfo = new List<InvoicePaymentModel>()               
                {
                    new InvoicePaymentModel { InvoiceId ="21EC2020-3AEA-4069-A2DD-08002B30309D", InvoiceNo="I021", InvoiceDate = Convert.ToDateTime("2016-01-01 10:09:03"), PaymentId = "P21", PaymentDate = Convert.ToDateTime("2016-01-02 08:09:03"), PaymentMethod ="CC", PaymentAmount = 45 , BalanceAmount = 0},
                    new InvoicePaymentModel { InvoiceId ="23EC2020-3AEA-4069-A2DD-08002B30309D", InvoiceNo="I023", InvoiceDate = Convert.ToDateTime("2016-01-01 11:09:03"), PaymentId = "P23", PaymentDate = Convert.ToDateTime("2016-01-02 11:10:03"), PaymentMethod ="CC", PaymentAmount = 45 , BalanceAmount = 0},
                };

                _svc = new BillxService(this.textBox3.Text.Trim(), BillxResource.WebApi.OTHERS);

                if (_svc.UpdateInvoicePaymentInfo(paymentinfo))
                    MessageBox.Show("Updated sucessfully");
                else
                    MessageBox.Show("Unable to update, check log");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                var vendors = new List<VendorModel>()               
                {
                    new VendorModel { ABN = "32154115458", Email = "vendor1@billxtest.com.au", Phone = "0292994747", VendorBusinessName = "MS.Vendor-1", VendorName = "Vendor-1", VendorType = "S" },
                    new VendorModel { ABN = "32154115359", Email = "vendor2@billxtest.com.au", Phone = "0212994549", VendorBusinessName = "MS.Vendor-2", VendorName = "Vendor-2", VendorType = "S" }
                };



                _svc = new BillxService(this.textBox3.Text.Trim(), BillxResource.WebApi.OTHERS);

                if (_svc.UploadVendors(vendors))
                    MessageBox.Show("Uploaded sucessfully");
                else
                    MessageBox.Show("Unable to upload, check log");


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                var invoiceIds = new List<string> { "21EC2020-3AEA-4069-A2DD-08002B30309D", "23EC2020-3AEA-4069-A2DD-08002B30309D" };

                _svc = new BillxService(this.textBox3.Text.Trim(), BillxResource.WebApi.OTHERS);

                if (_svc.DeleteInvoices(invoiceIds))
                    MessageBox.Show("Deleted sucessfully");
                else
                    MessageBox.Show("Unable to delete, check log");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                var logfile = ((Hierarchy)LogManager.GetRepository())
                                         .Root.Appenders.OfType<FileAppender>()
                                         .FirstOrDefault().File;

                if (!string.IsNullOrEmpty(logfile))
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo(logfile);
                    Process.Start(startInfo);
             
                }
                else
                {
                    MessageBox.Show("Please check log4net configuration");
                }
                               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        
    }
}
